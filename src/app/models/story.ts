export interface Story {
  id?: number;
  title?: string;
  content?: string;
  creationDate?: string;
  validated?: boolean;
  idResponse?: number;
}
