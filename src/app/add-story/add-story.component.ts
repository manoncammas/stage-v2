import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Story } from '../models/story';
import { StoryService } from '../services/story.service';

@Component({
  selector: 'app-add-story',
  templateUrl: './add-story.component.html',
  styleUrls: ['./add-story.component.css']
})
export class AddStoryComponent implements OnInit, OnDestroy {

  stories: Story[] = [];

  submitted = false;

  storyForm = new FormGroup({
    title: new FormControl ('', Validators.required),
    content: new FormControl('', Validators.required),
    });

  constructor(private storyService: StoryService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getStories();
  }

  get f() { return this.storyForm.controls; }

  ngOnDestroy(): void {
    this.storyService.getStories()
    .subscribe(stories  => this.stories = stories)
    .unsubscribe();
  }

  getStories(): void {
    this.storyService.getStories()
    .subscribe(stories  => this.stories = stories);
  }

  add(title: string, content: string): void {
    this.submitted = true;

    // stop here if form is invalid
    if (this.storyForm.invalid) {
        return;
    }

    title = title.trim();
    content = content.trim();
    if (!content && !title) { return; }
    this.storyService.addStory({ title, content } as Story)
      .subscribe(story => {
        this.stories.push(story);
      });
  }
}
