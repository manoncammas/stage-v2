import { Component, OnDestroy, OnInit } from '@angular/core';

import { Story } from '../models/story';
import { Response } from '../models/response';
import { StoryService } from '../services/story.service';
import { ResponseService } from '../services/response.service';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';

@Component({
  selector: 'app-random-mode',
  templateUrl: './random-mode.component.html',
  styleUrls: ['./random-mode.component.css']
})
export class RandomModeComponent implements OnInit, OnDestroy {

  responses: Response[] = [];

  randomStory?: Story; // histoire selectionee aléatoirement
  unrespondedStories: Story[] = []; // tableau des histoires sans réponses
  userResponse?: Response = {};

  submitted = false;

  responseForm = new FormGroup({
    content: new FormControl('', Validators.required),
    });

  constructor(private storyService: StoryService, private responseService: ResponseService, private formBuilder: FormBuilder) { }

  ngOnInit(): void {
    this.getResponses();
    this.getUnrespondedStories();
  }

  get f() { return this.responseForm.controls; }

  ngOnDestroy(): void {
    this.responseService.getResponses()
      .subscribe(responses  => this.responses = responses)
      .unsubscribe();
    console.log('OnDestroy');
    this.storyService.getUnrespondedStories()
      .subscribe(unrespondedStories => this.unrespondedStories = unrespondedStories)
      .unsubscribe();
  }

  random(): void { // fonction pour prendre une histoire au hasard parmi celles sans réponse
    this.randomStory = this.unrespondedStories[Math.floor(Math.random() * this.unrespondedStories.length)];
  }

  getResponses(): void {
    this.responseService.getResponses()
    .subscribe(responses  => this.responses = responses);
  }

  submit(content: string): Promise<void> {
    this.submitted = true;

    // stop here if form is invalid
    if (this.responseForm.invalid) {
        return;
    }

    content = content.trim();
    if (!content) {return; }
    this.userResponse = this.responseService.createResponse(content, this.randomStory);
    this.responseService.postResponse(this.userResponse)
      .subscribe(response => {
        this.responses.push(response);
      });
    this.responseService.linkStoryWithResponse(this.userResponse.idStory);
    this.getUnrespondedStories();
    this.randomStory = null;
    this.userResponse = {};
  }

  getUnrespondedStories(): void  { // fonction appelée à l'init pour filtrer les histoires
    this.storyService.getUnrespondedStories().subscribe(unrespondedStories => this.unrespondedStories = unrespondedStories);
  }

}

