import { Component, OnDestroy, OnInit } from '@angular/core';

import { Story } from '../models/story';
import { StoryService } from '../services/story.service';
import { Response } from '../models/response';
import { ResponseService } from '../services/response.service';
import { concat, Observable } from 'rxjs';
import { FormControl, FormGroup } from '@angular/forms';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  stories$: Observable<Story[]>;
  // responses: Response[] = [];

  selectedStory?: Story; // histoire selectionee pour afficher les details
  selectedResponse?: Response; // reponse correspondant à l'histoire selectionee

  profileForm = new FormGroup({
    validated: new FormControl (false),
    answered: new FormControl(false),
    all: new FormControl(false),
    });

  constructor(private storyService: StoryService, private responseService: ResponseService) { }

  ngOnInit(): void {
    this.stories$ = this.storyService.getStories();
    // this.getResponses();
  }

  onSubmit(): void {
    // TODO: Use EventEmitter with form value
    console.log(this.profileForm.value);
  }

  onSelect(story: Story): void { // enregistre l'histoire et la reponse selectionnees
    this.selectedStory = story;
    // this.selectedResponse = this.responses.find(response => response.idStory === this.selectedStory.id);
    this.responseService.getResponseFromIdStory(this.selectedStory.id)
      .subscribe(response => this.selectedResponse = response);
    console.log('selectedStory', this.selectedStory, 'selectedResponse', this.selectedResponse);
  }

  /* getResponses(): void {
    this.responseService.getResponses()
    .subscribe(responses  => this.responses = responses);
  }*/

  filterDashboard(val: boolean, res: boolean, all: boolean): void {
    if (all === false)
    {
      if (val === true)
      {
        if (res === true) {
          this.stories$ = concat(this.storyService.getRespondedStories(), this.storyService.getValidatedStories());
        }
        else if (res === false) {
          this.stories$ = this.storyService.getValidatedStories();
        }
      }
      else if (val === false) {
        if (res === true) {
          this.stories$ = this.storyService.getRespondedStories();
        }
        else if (res === false) {
          this.stories$ = this.storyService.getStories();
        }
      }
    }
    else {
      this.stories$ = this.storyService.getStories();
    }
  }

  onChange(val: boolean, res: boolean, all: boolean): void {
    this.filterDashboard(val, res, all);
    console.log('validated: ', val);
    console.log('answered: ', res);
    console.log('all: ', all);
  }

}

