import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { concat, Observable, of } from 'rxjs';
import { Story } from '../models/story';
import { catchError, filter, map, tap } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class StoryService {

  private storiesUrl = 'api/stories';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient) { }

  /** GET stories from the server */
  getStories(): Observable<Story[]> {
  return this.http.get<Story[]>(this.storiesUrl)
    .pipe(
      tap((data) => console.log('getStories', data)),
      catchError(this.handleError<Story[]>('getStories', []))
    );
  }

  /** PUT: update the story on the server */
  updateStory(story: Story): Observable<any> {
    return this.http.put(this.storiesUrl, story, this.httpOptions)
    .pipe(
      tap(_ => console.log(`updated story id=${story.id}`)),
      catchError(this.handleError<any>('updateStory'))
    );
  }

  /** POST: add a new story to the server */
  addStory(story: Story): Observable<Story> {
    return this.http.post<Story>(this.storiesUrl, story, this.httpOptions).pipe(
      tap((newStory: Story) => console.log(`added story id=${newStory.id}`)),
      catchError(this.handleError<Story>('addStory'))
    );
  }

  getUnrespondedStories(): Observable<Story[]>  {
    return this.getStories()
      .pipe(
        map((stories: Story[]) => stories.filter(story => !story.idResponse)));
  }

  getValidatedStories(): Observable<Story[]> {
    return this.getStories()
      .pipe(
        map((stories: Story[]) => stories.filter(story => story.validated === true)));
  }

  getRespondedStories(): Observable<Story[]> {
    return this.getStories()
      .pipe(
        map((stories: Story[]) => stories.filter(story => story.idResponse)));
  }

  // tslint:disable-next-line: typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
  /* GET heroes whose name contains search term */
    respondedStories(term: boolean): Observable<Story[]> {
      if (term === true) {
        return this.getRespondedStories();
      }
    }
}
