import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, tap, map, pluck } from 'rxjs/operators';
import { Response } from '../models/response';
import { Story } from '../models/story';
import { StoryService } from './story.service';
import { combineLatest } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ResponseService {

  private responsesUrl = 'api/responses';  // URL to web api

  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  constructor(private http: HttpClient, private storyService: StoryService) { }

  /** GET heroes from the server */
  getResponses(): Observable<Response[]> {
  return this.http.get<Response[]>(this.responsesUrl)
    .pipe(
      tap((data) => console.log('getResponses', data)),
      catchError(this.handleError<Response[]>('getResponses', []))
    );
  }

  /** POST: add a new response to the server */
  postResponse(response: Response): Observable<Response> {
    return this.http.post<Response>(this.responsesUrl, response, this.httpOptions).pipe(
      tap((newResponse: Response) => console.log(`added response id=${newResponse.id}`)),
      catchError(this.handleError<Response>('createResponse'))
    );
  }

  getResponseFromIdStory(id: number): Observable<Response> {
    return this.getResponses()
      .pipe(
        map((responses: Response[]) => responses.find(response => response.idStory === id))
      );
  }

  createResponse(content: string, story: Story): Response {
    const newResponse = { content } as Response;
    newResponse.idStory = story.id;
    return newResponse;
  }

  linkStoryWithResponse(idStory: number): void {
    combineLatest(this.getResponses(), this.storyService.getStories()).subscribe(
      ([responses, stories]) => {
        const response = responses.find(r => r.idStory === idStory);
        const story = stories.find(s => s.id === response.idStory);
        story.idResponse = response.id;
        this.storyService.updateStory(story).subscribe();
      });
  }

  // tslint:disable-next-line: typedef
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
