import { Injectable } from '@angular/core';
import { InMemoryDbService } from 'angular-in-memory-web-api';
import { Story } from '../models/story';
import { Response } from '../models/response';

@Injectable({
  providedIn: 'root',
})
export class InMemoryDataService implements InMemoryDbService {
  // tslint:disable-next-line: typedef
  createDb() {
    const stories = [
      {
        id: 1,
        title: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
        content: 'Lorem ipsum dolor sit amet. Aut quia dicta rem nulla dolor sunt possimus. Aut harum nulla ea facere in galisum minima ut dolor minima.\
        Aut eligendi dolores et libero dolore ad sequi culpa id consequatur nihil a inventore dolores qui reprehenderit consequuntur eum accusamus adipisci.\
        A iure aliquid quo nihil similique sed fuga accusantium et explicabo totam et aliquid consectetur est soluta aspernatur est earum minima.\
        Et ducimus consectetur ab iste voluptatem et quod magnam eum nihil autem est odio similique 33 quasi galisum.\
        Ex consectetur iste hic ratione commodi aut nulla quia rem quod galisum eos exercitationem consequatur qui optio rerum.\
        Aut delectus debitis eos iste impedit ex delectus quia ut repellendus odit aut ipsum repudiandae ut consequatur vero!\
        Ut expedita saepe ab minima asperiores et molestiae quisquam et dolor consequatur et dicta numquam.\
        Et vitae doloribus cum autem quae a maiores ipsa est maxime dolorem vel veniam error ex quasi culpa aut corrupti sint?\
        Sed necessitatibus doloribus ut expedita aliquam et amet eius eos nemo voluptatem qui alias sint.\
        Ea consequatur possimus est odit deserunt et amet aliquid eos sunt assumenda. Et voluptatum ratione ut esse consequatur hic illo omnis qui laborum galisum et optio dolore sed suscipit officiis.',
        creationDate: '01/01/01',
        validated: false,
        idResponse: 1,
      },
      {
        id: 2,
        title: 'Titre histoire 2',
        content: 'Ceci est l\'histoire numero 2',
        creationDate: '02/02/02',
        validated: false,
        idResponse: 2
      },
      {
        id: 3,
        title: 'Titre histoire 3',
        content: 'Ceci est l\'histoire numero 3',
        creationDate: '03/03/03',
        validated: false,
        idResponse: 3
      },
      {
        id: 4,
        title: 'Titre histoire 4',
        content: 'Ceci est l\'histoire numero 4',
        creationDate: '04/04/04',
        validated: false,
        idResponse: 4
      },
      {
        id: 5,
        title: 'Titre histoire 5',
        content: 'Ceci est l\'histoire numero 5',
        creationDate: '05/05/05',
        validated: false,
        idResponse: 5
      },
      {
        id: 6,
        title: 'Titre histoire 6',
        content: 'Ceci est l\'histoire numero 6',
        creationDate: '06/06/06',
        validated: false,
        idResponse: 6
      },
      {
        id: 7,
        title: 'Titre histoire 7',
        content: 'Ceci est l\'histoire numero 7',
        creationDate: '07/07/07',
        validated: false
      },
      {
        id: 8,
        title: 'Titre histoire 8',
        content: 'Ceci est l\'histoire numero 8',
        creationDate: '08/08/08',
        validated: false
      },
      {
        id: 9,
        title: 'Titre histoire 9',
        content: 'Ceci est l\'histoire numero 9',
        creationDate: '09/09/09',
        validated: false
      }
    ];

    const responses = [
      {
        id: 1,
        content: 'Aut delectus debitis eos iste impedit ex delectus quia ut repellendus odit aut ipsum repudiandae ut consequatur vero!\
        Ut expedita saepe ab minima asperiores et molestiae quisquam et dolor consequatur et dicta numquam.\
        Et vitae doloribus cum autem quae a maiores ipsa est maxime dolorem vel veniam error ex quasi culpa aut corrupti sint?\
        Sed necessitatibus doloribus ut expedita aliquam et amet eius eos nemo voluptatem qui alias sint.\
        Ea consequatur possimus est odit deserunt et amet aliquid eos sunt assumenda. Et voluptatum ratione ut esse consequatur hic illo omnis qui laborum galisum et optio dolore sed suscipit officiis.',
        idStory: 1,
      },
      {
        id: 2,
        content: 'Ceci est la reponse a l\'histoire 2',
        idStory: 2,
      },
      {
        id: 3,
        content: 'Ceci est la reponse a l\'histoire 3',
        idStory: 3,
      },
      {
        id: 4,
        content: 'Ceci est la reponse a l\'histoire 4',
        idStory: 4,
      },
      {
        id: 5,
        content: 'Ceci est la reponse a l\'histoire 5',
        idStory: 5,
      },
      {
        id: 6 ,
        content: 'Ceci est la reponse a l\'histoire 6',
        idStory: 6,
      }
    ];
    return {stories, responses};
  }

  genIdStories(stories: Story[]): number {
    return stories.length > 0 ? Math.max(...stories.map(story => story.id)) + 1 : 1;
  }

  genIdResponses(responses: Response[]): number {
    return responses.length > 0 ? Math.max(...responses.map(response => response.id)) + 1 : 1;
  }
}
