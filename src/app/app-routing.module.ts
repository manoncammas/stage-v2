import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddStoryComponent } from './add-story/add-story.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { RandomModeComponent } from './random-mode/random-mode.component';

const routes: Routes = [
  { path: '', redirectTo: '/dashboard', pathMatch: 'full' },
  { path: 'dashboard', component: DashboardComponent },
  { path: 'random', component: RandomModeComponent },
  { path: 'add', component: AddStoryComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
